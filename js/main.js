
$( document ).ready(function() { 


    $("#about-welcome").show().addClass("animated fadeInRightBig");
    $("#about-description").show().addClass("animated fadeInLeftBig");
    $("#about-technologies").show().addClass("animated fadeInRightBig");
    $("#about-technologies-list1").show().addClass("animated fadeInLeftBig");
    $("#about-technologies-list2").show().addClass("animated fadeInRightBig");
    // $("#about-welcome").addClass("animated fadeInRightBig");
    // $("#about-description").addClass("animated fadeInLeftBig");
    // $("#about-technologies").addClass("animated fadeInRightBig");
    // $("#about-technologies-list1").addClass("animated fadeInLeftBig");
    // $("#about-technologies-list2").addClass("animated fadeInRightBig");

    $("#projects-header").addClass("animated fadeInRightBig");
    $("#projects-list").addClass("animated fadeInLeftBig");

    $('#nav-button').click(function() {

    //menu
        console.log('click')
      //  $('#main-menu').toggleClass('hide-menu');
        $("#main-menu").slideToggle("slow");
        $('[data-chaffle]').trigger( 'mouseenter' );
        
    });
    
    //chaffle
    $('[data-chaffle]').each(function() {
        console.log(this);
        var chaffle = new Chaffle(this,{
            speed: 60,
            delay: 120
        })
        $(this).parent().mouseenter(function() {
            chaffle.init();
        });
    });

    // about me
    $('#about-button').click(function() {
        window.history.pushState('o-mnie', 'O mnie', '?fragment=o-mnie');    

        $("#main-menu").slideUp("slow");
        $("#about").show().addClass("animated flipInX");

        $('#about').one('webkitAnimationEnd oanimationend msAnimationEnd animationend',
        function(e) {
        
            // nastepne rzeczy
            $("#about-welcome").addClass("animated fadeInRightBig");
            $("#about-description").addClass("animated fadeInLeftBig");
            $("#about-technologies").addClass("animated fadeInRightBig");
            $("#about-technologies-list1").addClass("animated fadeInLeftBig");
            $("#about-technologies-list2").addClass("animated fadeInRightBig");
    
        });
       
        
        $('#contact').hide();
        $('#projects').hide();
      
    });

    // projects
    $('#projects-button').click(function() {
        window.history.pushState('portfolio', 'Portfolio', '?fragment=portfolio');     

        $("#main-menu").slideUp("slow");
        $("#projects").show().addClass("animated flipInX");

        $('#projects').one('webkitAnimationEnd oanimationend msAnimationEnd animationend',
        function(e) {
            $("#projects-header").addClass("animated fadeInRightBig");
            $("#projects-list").addClass("animated fadeInLeftBig");
        });

        $('#about').slideUp('hide');
        $('#contact').slideUp('hide');
      
    });

    


    //project content
    $('.project-miniature').click(function() {
        var id = $(this).attr('id');
        document.getElementById(id+'-overlay').style.width = "100vw";
        //$('#'+id+'-overlay').width("100%");
        
        setTimeout(function(){ 
            
            $('#'+id+'-skitter').skitter({
                navigation: true,
                velocity: 1,
                dots: true,              
            });

         }, 1500);
    });

    $('.closebtn').click(function() {
        $(this).parent().width("0%");        
    });


    //contact
    
    $('#contact-button').click(function() {
        window.history.pushState('kontakt', 'Kontakt', '?fragment=kontakt');    
       
        $("#main-menu").slideUp("slow");
        $("#contact").slideDown("slow");

        $('#about').slideUp('hide');
        $('#projects').slideUp('hide');
      
    });


 }); 



