<?php
/* Smarty version 3.1.30, created on 2017-12-07 11:46:17
  from "/var/www/lighttpd/Basia/templates/contact.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5a291bf9a7a658_22370288',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '30acee6b389e5dfe5d215942c6817e6bc4ba09ff' => 
    array (
      0 => '/var/www/lighttpd/Basia/templates/contact.tpl',
      1 => 1512643387,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a291bf9a7a658_22370288 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="center-contact">
    
            <h2>Kontakt</h2></br>

            <form action="?fragment=kontakt&action=send-mail" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Imię" required>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Telefon" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" id="content" rows="5" name="content" placeholder="Wiadomość" required></textarea>
                </div></br>
                
                <button type="submit" name="submit" value="submit" class="btn btn-info">Wyślij</button>
            </form>
        
</div><?php }
}
