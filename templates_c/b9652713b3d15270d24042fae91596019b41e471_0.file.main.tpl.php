<?php
/* Smarty version 3.1.30, created on 2017-12-04 11:00:39
  from "/var/www/lighttpd/Basia/templates/main.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5a251cc7bccf91_11704073',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b9652713b3d15270d24042fae91596019b41e471' => 
    array (
      0 => '/var/www/lighttpd/Basia/templates/main.tpl',
      1 => 1512381631,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:about.tpl' => 1,
    'file:projects.tpl' => 1,
    'file:contact.tpl' => 1,
  ),
),false)) {
function content_5a251cc7bccf91_11704073 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="" content="">
    <meta name="author" content="Barbara Lewicka">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Barbara Lewicka | web dev</title>

    <link rel="icon" type="image/png" href="images/icons/favicon.png">
    <link rel="shortcut icon" href="images/icons/favicon.png" type="image/png"/>

  <!-- css -->
  <link rel="stylesheet" type="text/css" href="libs/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="libs/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" type="text/css" href="libs/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" type="text/css" href="libs/ionicons-2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" type="text/css" href="libs/tether-1.3.3/css/tether.min.css">
  <link rel="stylesheet" type="text/css" href="libs/overlay/overlay.css">
  <link rel="stylesheet" type="text/css" href="libs/animate/animate.css">
  <link rel="stylesheet" type="text/css" href="libs/slideshow/skitter.css">

  <link rel="stylesheet" type="text/css" href="css/carousel.css">



  <!-- Custom css -->
  <link rel="stylesheet" type="text/css" href="css/main.css">

  <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">

 
  </head>

  <body>

    <div id="nav-button">
      <i id="nav-lines" class="ionicons ion-android-menu"></i>
    </div>
 
    <div class="hide" id="main-menu">
        <div id="center-menu">

                  
                  <div class="menu-button"><a href="#" class="menu-link" id="about-button" data-chaffle="en">#o-mnie</a>
                  </div>
                  <div class="menu-button"><a href="#" class="menu-link" id="projects-button" data-chaffle="en">#portfolio</a>
                  </div>
                  <div class="menu-button"><a href="#" class="menu-link" id="contact-button" data-chaffle="en">#kontakt</a>
                  </div>


        </div>
    </div>
    
  <div id="container">
      <div class="hide" id="about" style="<?php if ($_smarty_tpl->tpl_vars['fragment']->value == 'o-mnie') {?>display: block;<?php }?>">
        <?php $_smarty_tpl->_subTemplateRender("file:about.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      </div>

      <div class="hide" id="projects" style="<?php if ($_smarty_tpl->tpl_vars['fragment']->value == 'portfolio') {?>display: block;<?php }?>">
        <?php $_smarty_tpl->_subTemplateRender("file:projects.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      </div>

      <div class="hide" id="contact" style="<?php if ($_smarty_tpl->tpl_vars['fragment']->value == 'kontakt') {?>display: block;<?php }?>">
        <?php $_smarty_tpl->_subTemplateRender("file:contact.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      </div>
  </div>





  <!-- JS -->
  <?php echo '<script'; ?>
 src="libs/bootstrap/js/jquery-3.2.1.min.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="libs/tether-1.3.3/js/tether.min.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="libs/bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>

  <?php echo '<script'; ?>
 src="libs/chaffle/chaffle.min.js"><?php echo '</script'; ?>
>

  <?php echo '<script'; ?>
 src="libs/slideshow/jquery.easing.1.3.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="libs/slideshow/jquery.skitter.min.js"><?php echo '</script'; ?>
>

  <?php echo '<script'; ?>
 src="js/carousel.js"><?php echo '</script'; ?>
>

  

  




  <!-- custom JS -->
  <?php echo '<script'; ?>
 src="js/main.js"><?php echo '</script'; ?>
>

  
  
   
  </body>
</html> 
<?php }
}
