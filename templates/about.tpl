<div id="center-about">

    {* <img class="profile-pic" alt="My photo" src="icons/profile.jpg">
    <br><br><br> *}
    <h1 id="about-welcome"> Witaj </h1>
    <br>
    
    <p id="about-description">Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus orci luctus non, consectetuer lobortis quis, varius in, purus. Integer ultrices posuere cubilia </p><br><br><br>
    
    <h2 id="about-technologies"> Poznane technologie </h2><br><br>

    <div id="carousel">
        
        <div class="hideLeft">
            <img class="tech-icon" src="images/icons/php.png" /><br>
            <div class="grading">
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star-outline"></i>
                <i class="ionicons ion-ios-star-outline"></i>
                <i class="ionicons ion-ios-star-outline"></i>
            </div>
        </div>
       

        <div class="prevLeftSecond">
            <img class="tech-icon" src="images/icons/html.png" /><br>
            <div class="grading">
                    <i class="ionicons ion-ios-star"></i>
                    <i class="ionicons ion-ios-star"></i>
                    <i class="ionicons ion-ios-star"></i>
                    <i class="ionicons ion-ios-star-half"></i>
                    <i class="ionicons ion-ios-star-outline"></i>
            </div>
        </div>

        <div class="prev">
            <img class="tech-icon" src="images/icons/css.png" /><br>
            <div class="grading">
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star-outline"></i>
                <i class="ionicons ion-ios-star-outline"></i> 
            </div>
        </div>

        <div class="selected">
            <img class="tech-icon" src="images/icons/bootstrap.png" /><br>
            <div class="grading">
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star-half"></i>
                <i class="ionicons ion-ios-star-outline"></i>
            </div>
        </div>

      <div class="next">
            <img class="tech-icon" src="images/icons/smarty.png" /><br>
            <div class="grading">
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star-outline"></i>
                <i class="ionicons ion-ios-star-outline"></i> 
            </div>
      </div>

      <div class="nextRightSecond">
            <img class="tech-icon" src="images/icons/sql.png" /><br>
            <div class="grading">
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star-half"></i>
                <i class="ionicons ion-ios-star-outline"></i>
                <i class="ionicons ion-ios-star-outline"></i>
                <i class="ionicons ion-ios-star-outline"></i>
            </div>
      </div>

      <div class="hideRight">
            <img class="tech-icon" src="images/icons/psd.png" /><br>
            <div class="grading">
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star"></i>
                <i class="ionicons ion-ios-star-half"></i>
                <i class="ionicons ion-ios-star-outline"></i>
            </div>
      </div>     
    
    </div>

    <div class="carousel-buttons">
        <a id="prev"><i class="ionicons ion-ios-arrow-left"></i></a>
        <a id="next"><i class="ionicons ion-ios-arrow-right"></i></a>
    </div>


</div>
