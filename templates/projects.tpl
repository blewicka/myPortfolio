<div id="center-projects">
    <div id="projects-header">
   <h1> Projekty </h1></br>
    </div>
    <div class="row" id="projects-list">

        {* projekt 1 *}
        <div class="col-xs-12 col-sm-12 col-lg-6" >            
            <span style="font-size:30px;cursor:pointer"><img class="project-miniature" id="project1" src="images/projekt1/A1.png" alt="Smiley face">  </span>           
        </div>  

       {* projekt 2 *}
        <div class="col-xs-12 col-sm-12 col-lg-6" > 
            <span style="font-size:30px;cursor:pointer"><img class="project-miniature" id="project2" src="images/projekt2/projekt2.png" alt="Smiley face">  </span>           
        </div>
      
        {* projekt 3 *}
        <div class="col-xs-12 col-sm-12 col-lg-6" > 
            <span style="font-size:30px;cursor:pointer"><img class="project-miniature" id="project3" src="icons/profile.jpg" alt="Smiley face">  </span>           
        </div>
        

    </div>

</div>

        {* projekt 1 *}
        <div id="project1-overlay" class="overlay">
                <a href="javascript:void(0)" class="closebtn">&times;</a>
                <div class="overlay-content">
                    <div class="center-project">


                        <div class="skitter-large-box" >
                            <div class="skitter skitter-large with-dots" id="project1-skitter">
                                <ul>
                                    <li><a href="http://aisab.duckdns.org/Agata/" target="_blank"><img src="images/projekt1/A1.png" class="cube" /></a><div class="label_text">Strona główna</div></li>
                                    <li><a href="http://aisab.duckdns.org/Agata/" target="_blank"><img src="images/projekt1/A2.png" class="cube" /></a><div class="label_text">Strona główna</div></li> 
                                    <li><a href="http://aisab.duckdns.org/Agata/" target="_blank"><img src="images/projekt1/A3.png" class="cube" /></a><div class="label_text">Panel administratora</div></li> 
                                    <li><a href="http://aisab.duckdns.org/Agata/" target="_blank"><img src="images/projekt1/A4.png" class="cube" /></a><div class="label_text">Panel administratora</div></li> 
                                    <li><a href="http://aisab.duckdns.org/Agata/" target="_blank"><img src="images/projekt1/A5.png" class="cube" /></a><div class="label_text">Panel administratora</div></li> 
                                </ul>
                            </div>

                        </div> 
                        <div class="row">
                            <div class="col"><img class="tech-icon" src="images/icons/php.png" /><br>  </div>
                            <div class="col"><img class="tech-icon" src="images/icons/css.png" /><br>  </div>
                            <div class="col"><img class="tech-icon" src="images/icons/html.png" /><br> </div>
                            <div class="col"><img class="tech-icon" src="images/icons/sql.png" /><br> </div>
                            <div class="col"><img class="tech-icon" src="images/icons/psd.png" /><br>  </div>
                            
                        </div></br>
                        <p> Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus orci luctus non, consectetuer lobortis quis, varius in, purus. Integer ultrices posuere cubilia Curae, Nulla ipsum dolor lacus, suscipit adipiscing. Cum sociis natoque penatibus et ultrices volutpat. Nullam wisi ultricies a, gravida vitae, dapibus risus ante sodales lectus blandit eu, tempor diam pede cursus vitae, ultricies eu, faucibus quis, porttitor eros cursus lectus, pellentesque eget, bibendum a, gravida ullamcorper quam. Nullam viverra consectetuer. Quisque cursus et, porttitor risus. Aliquam sem. In hendrerit nulla quam nunc, accumsan congue. Lorem ipsum primis in nibh vel risus. Sed vel lectus. Ut sagittis, ipsum dolor quam. </p>
                        
                    </div>
                </div>
            </div>
        


        {* projekt 2 *}       
        <div id="project2-overlay" class="overlay">
            <a href="javascript:void(0)" class="closebtn">&times;</a>
            <div class="overlay-content">
                <div class="center-project">


                    <div class="skitter-large-box" >
                        <div class="skitter skitter-large with-dots" id="project2-skitter">
                            <ul>
                                <li><a href="https://e-vipol.pl" target="_blank"><img src="images/projekt2/projekt2.png" class="cube" /></a><div class="label_text">Strona główna</div></li>
                                {* <li><a href="http://aisab.duckdns.org/Agata/" target="_blank"><img src="images/projekt1/A2.png" class="cube" /></a><div class="label_text">Strona główna</div></li>  *}
                            </ul>
                        </div>

                    </div> 
                    <div class="row">
                        <div class="col"><img class="tech-icon" src="images/icons/php.png" /><br>  </div>
                        <div class="col"><img class="tech-icon" src="images/icons/css.png" /><br>  </div>
                        <div class="col"><img class="tech-icon" src="images/icons/html.png" /><br> </div>
                        <div class="col"><img class="tech-icon" src="images/icons/sql.png" /><br> </div>
                        <div class="col"><img class="tech-icon" src="images/icons/psd.png" /><br>  </div>
                        
                    </div></br></br>
                    <p> Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus orci luctus non, consectetuer lobortis quis, varius in, purus. Integer ultrices posuere cubilia Curae, Nulla ipsum dolor lacus, suscipit adipiscing. Cum sociis natoque penatibus et ultrices volutpat. Nullam wisi ultricies a, gravida vitae, dapibus risus ante sodales lectus blandit eu, tempor diam pede cursus vitae, ultricies eu, faucibus quis, porttitor eros cursus lectus, pellentesque eget, bibendum a, gravida ullamcorper quam. Nullam viverra consectetuer. Quisque cursus et, porttitor risus. Aliquam sem. In hendrerit nulla quam nunc, accumsan congue. Lorem ipsum primis in nibh vel risus. Sed vel lectus. Ut sagittis, ipsum dolor quam. </p>
                    
                </div>
            </div>
        </div>

         {* projekt 3 *}
        <div id="project3-overlay" class="overlay">
            <a href="javascript:void(0)" class="closebtn">&times;</a>
            <div class="overlay-content">
                test3
            </div>
        </div>
