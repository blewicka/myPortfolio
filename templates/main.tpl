<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="" content="">
    <meta name="author" content="Barbara Lewicka">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Barbara Lewicka | web dev</title>

    <link rel="icon" type="image/png" href="images/icons/favicon.png">
    <link rel="shortcut icon" href="images/icons/favicon.png" type="image/png"/>

  <!-- css -->
  <link rel="stylesheet" type="text/css" href="libs/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="libs/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" type="text/css" href="libs/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" type="text/css" href="libs/ionicons-2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" type="text/css" href="libs/tether-1.3.3/css/tether.min.css">
  <link rel="stylesheet" type="text/css" href="libs/overlay/overlay.css">
  <link rel="stylesheet" type="text/css" href="libs/animate/animate.css">
  <link rel="stylesheet" type="text/css" href="libs/slideshow/skitter.css">

  <link rel="stylesheet" type="text/css" href="css/carousel.css">



  <!-- Custom css -->
  <link rel="stylesheet" type="text/css" href="css/main.css">

  <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">

 
  </head>

  <body>

    <div id="nav-button">
      <i id="nav-lines" class="ionicons ion-android-menu"></i>
    </div>
 
    <div class="hide" id="main-menu">
        <div id="center-menu">

                  
                  <div class="menu-button"><a href="#" class="menu-link" id="about-button" data-chaffle="en">#o-mnie</a>
                  </div>
                  <div class="menu-button"><a href="#" class="menu-link" id="projects-button" data-chaffle="en">#portfolio</a>
                  </div>
                  <div class="menu-button"><a href="#" class="menu-link" id="contact-button" data-chaffle="en">#kontakt</a>
                  </div>


        </div>
    </div>
    
  <div id="container">
      <div class="hide" id="about" style="{if $fragment == 'o-mnie'}display: block;{/if}">
        {include file='about.tpl'}
      </div>

      <div class="hide" id="projects" style="{if $fragment == 'portfolio'}display: block;{/if}">
        {include file='projects.tpl'}
      </div>

      <div class="hide" id="contact" style="{if $fragment == 'kontakt'}display: block;{/if}">
        {include file='contact.tpl'}
      </div>
  </div>





  <!-- JS -->
  <script src="libs/bootstrap/js/jquery-3.2.1.min.js"></script>
  <script src="libs/tether-1.3.3/js/tether.min.js"></script>
  <script src="libs/bootstrap/js/bootstrap.min.js"></script>

  <script src="libs/chaffle/chaffle.min.js"></script>

  <script src="libs/slideshow/jquery.easing.1.3.js"></script>
  <script src="libs/slideshow/jquery.skitter.min.js"></script>

  <script src="js/carousel.js"></script>

  

  




  <!-- custom JS -->
  <script src="js/main.js"></script>

  
  
   
  </body>
</html> 
